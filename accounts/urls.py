from django.urls import path
from accounts.views import login_view, logout_view, user_signup


urlpatterns = [
    path("login/", login_view, name="login"),
    path("logout/", logout_view, name="logout"),
    path("signup/", user_signup, name="signup"),
    path("accounts/home/", login_view, name="home"),
]
