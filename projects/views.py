from django.shortcuts import render, get_object_or_404, redirect
from .forms import Project, ProjectForm
from django.contrib.auth.decorators import login_required


# Create your views here.


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "accounts/login.html", context)


def list_projects(request):
    projects = Project.objects.all()
    context = {"projects": projects}
    return render(request, "project_list.html", context)


@login_required
def show_project(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    context = {"project": project}
    return render(request, "show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save()
            context = {"project": project}
            return redirect("project_list")
    else:
        form = ProjectForm()
    return render(request, "projects/create.html", {"form": form}, context)
