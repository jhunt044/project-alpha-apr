from django.urls import path
from .views import show_project, create_project, project_list


app_name = "projects"


urlpatterns = [
    path("", project_list, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_task"),
]
