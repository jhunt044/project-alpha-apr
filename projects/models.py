from django.db import models
from django.contrib.auth.models import User
from django.views.generic import ListView


# Create your models here.


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        User, related_name="projects", on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return self.name


class ProjectListView(ListView):
    model = Project
    template_name = "project_list.html"
    context_object_name = "projects"
