from .models import Project
from django import forms


class ProjectForm(forms.Form):
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]
