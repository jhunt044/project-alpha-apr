from django import forms
from .models import Task


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ["is_completed"]
        fields = ["name", "start_date", "due_date"]
